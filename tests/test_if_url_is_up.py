# Test imports
import pytest
from assertpy import assert_that, soft_assertions
from pytest import param

# Standard imports
from http import HTTPStatus

# Third party imports
import requests
import requests_mock

# First party imports
from exourlchecker import UrlValidator


@pytest.mark.parametrize(
    "url, status_code, expected",
    [
        param(
            "http://toto.com",
            code.value,
            (
                [
                    {
                        "link": {
                            "url": "http://toto.com",
                            "source": [{"text": "", "source": ""}],
                        },
                        "valid": True,
                    }
                ]
                if code in [HTTPStatus.OK, HTTPStatus.TEMPORARY_REDIRECT]
                else [
                    {
                        "link": {
                            "url": "http://toto.com",
                            "source": [{"text": "", "source": ""}],
                        },
                        "valid": False,
                        "status_code": code.value,
                    }
                ]
            ),
        )
        for code in HTTPStatus
    ],
)
def test_check_status_code(url, status_code, expected):
    with requests_mock.Mocker() as mocker:
        mocker.get(url, json={}, status_code=status_code)
        validator = UrlValidator(
            [{"url": url, "source": [{"text": "", "source": ""}]}],
            [HTTPStatus.OK, HTTPStatus.TEMPORARY_REDIRECT],
        )
        result = validator.check_requests()
    assert_that(result).is_equal_to(expected)


def test_check_status_code_404():
    with requests_mock.Mocker() as mocker:
        mocker.get("http://titi.com", json={}, status_code=404)
        validator = UrlValidator(
            [{"url": "http://titi.com", "source": [{"text": "", "source": ""}]}],
            [HTTPStatus.NOT_FOUND],
        )
        result = validator.check_requests()
        assert_that(result).is_equal_to(
            [
                {
                    "link": {
                        "url": "http://titi.com",
                        "source": [{"text": "", "source": ""}],
                    },
                    "valid": True,
                }
            ]
        )


def test_check_status_code_exception():
    with requests_mock.Mocker() as mocker:
        mocker.get("http://toto.com", exc=requests.exceptions.ConnectionError())

        validator = UrlValidator(
            [{"url": "http://toto.com", "source": [{"text": "", "source": ""}]}],
            [HTTPStatus.OK, HTTPStatus.TEMPORARY_REDIRECT],
        )
        result = validator.check_requests()
        assert_that(result).is_equal_to(
            [
                {
                    "link": {
                        "url": "http://toto.com",
                        "source": [{"text": "", "source": ""}],
                    },
                    "valid": False,
                    "error": "",
                }
            ]
        )


def test_check_status_code_exception_timeout():
    with requests_mock.Mocker() as mocker:
        mocker.get("http://toto.com", exc=requests.exceptions.Timeout())

        validator = UrlValidator(
            [{"url": "http://toto.com", "source": [{"text": "", "source": ""}]}],
            [HTTPStatus.OK],
        )
        result = validator.check_requests()
        assert_that(result).is_equal_to(
            [
                {
                    "link": {
                        "url": "http://toto.com",
                        "source": [{"text": "", "source": ""}],
                    },
                    "valid": False,
                    "status_code": HTTPStatus.REQUEST_TIMEOUT,
                }
            ]
        )


def test_status_code_is_none():
    with pytest.raises(ValueError, match="allowed_code cannot be None"):
        UrlValidator(
            [{"url": "http://toto.com", "source": [{"text": "", "source": ""}]}], None
        ).check_requests()
