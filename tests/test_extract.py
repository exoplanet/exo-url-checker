# Test imports
from assertpy import assert_that, soft_assertions

# First party imports
from exourlchecker import get_links


def test_extract_links():
    result = get_links("tests/testing_files/expected_links_for_extract.json")
    with soft_assertions():
        assert_that(result).is_not_empty()
        assert_that(result).contains_only(
            *[
                {
                    "url": "http://0.0.0.0:8100/2.html",
                    "text": "page 2",
                    "source": "http://0.0.0.0:8100/",
                },
                {
                    "url": "http://0.0.0.0:8100/3.html",
                    "text": "page 3",
                    "source": "http://0.0.0.0:8100/",
                },
                {
                    "url": "http://0.0.0.0:8100/4.html",
                    "text": "page 4",
                    "source": "http://0.0.0.0:8100/",
                },
                {
                    "url": "https://exoplanet.eu/",
                    "text": "exoplanet.eu",
                    "source": "http://0.0.0.0:8100/",
                },
                {
                    "url": "http://0.0.0.0:8100/index.html",
                    "text": "index",
                    "source": "http://0.0.0.0:8100/2.html",
                },
                {
                    "url": "http://0.0.0.0:8100/3.html",
                    "text": "page 3",
                    "source": "http://0.0.0.0:8100/2.html",
                },
                {
                    "url": "http://0.0.0.0:8100/4.html",
                    "text": "page 4",
                    "source": "http://0.0.0.0:8100/2.html",
                },
                {
                    "url": "https://exoplanet.eu/",
                    "text": "exoplanet.eu",
                    "source": "http://0.0.0.0:8100/2.html",
                },
                {
                    "url": "http://0.0.0.0:8100/index.html",
                    "text": "index",
                    "source": "http://0.0.0.0:8100/3.html",
                },
                {
                    "url": "http://0.0.0.0:8100/2.html",
                    "text": "page 2",
                    "source": "http://0.0.0.0:8100/3.html",
                },
                {
                    "url": "http://0.0.0.0:8100/4.html",
                    "text": "page 4",
                    "source": "http://0.0.0.0:8100/3.html",
                },
                {
                    "url": "https://exoplanet.eu/",
                    "text": "exoplanet.eu",
                    "source": "http://0.0.0.0:8100/3.html",
                },
                {
                    "url": "http://0.0.0.0:8100/index.html",
                    "text": "index",
                    "source": "http://0.0.0.0:8100/4.html",
                },
                {
                    "url": "http://0.0.0.0:8100/2.html",
                    "text": "page 2",
                    "source": "http://0.0.0.0:8100/4.html",
                },
                {
                    "url": "http://0.0.0.0:8100/3.html",
                    "text": "page 4",
                    "source": "http://0.0.0.0:8100/4.html",
                },
                {
                    "url": "https://exoplanet.eu/",
                    "text": "exoplanet.eu",
                    "source": "http://0.0.0.0:8100/4.html",
                },
                {
                    "url": "http://0.0.0.0:8100/2.html",
                    "text": "page 2",
                    "source": "http://0.0.0.0:8100/index.html",
                },
                {
                    "url": "http://0.0.0.0:8100/3.html",
                    "text": "page 3",
                    "source": "http://0.0.0.0:8100/index.html",
                },
                {
                    "url": "http://0.0.0.0:8100/4.html",
                    "text": "page 4",
                    "source": "http://0.0.0.0:8100/index.html",
                },
                {
                    "url": "https://exoplanet.eu/",
                    "text": "exoplanet.eu",
                    "source": "http://0.0.0.0:8100/index.html",
                },
            ]
        )
