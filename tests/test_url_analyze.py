# Test imports
import pytest
from assertpy import assert_that, soft_assertions
from pytest import param

# Standard imports
from http import HTTPStatus

# Third party imports
import requests_mock

# First party imports
from exourlchecker import UrlValidator, check_status_code, check_syntax


@pytest.mark.parametrize(
    "url, expected",
    [
        param(
            [{"url": "https:", "source": [{"text": "", "source": ""}]}],
            [
                {
                    "link": {"url": "https:", "source": [{"text": "", "source": ""}]},
                    "result": False,
                    "invalidate_regex": ["missing_double_slash", "empty_authority"],
                }
            ],
            id="Without double slash and empty authority.",
        ),
        param(
            [{"url": "", "source": [{"text": "", "source": ""}]}],
            [
                {
                    "link": {"url": "", "source": [{"text": "", "source": ""}]},
                    "result": False,
                    "invalidate_regex": [
                        "missing_protocol",
                        "missing_double_slash",
                        "empty_authority",
                    ],
                }
            ],
            id="Empty URL.",
        ),
        param(
            [{"url": "https://localhost/", "source": [{"text": "", "source": ""}]}],
            [
                {
                    "link": {
                        "url": "https://localhost/",
                        "source": [{"text": "", "source": ""}],
                    },
                    "result": False,
                    "invalidate_regex": ["local_authority"],
                }
            ],
            id="Local URL with https protocol.",
        ),
        param(
            [{"url": "http://localhost/", "source": [{"text": "", "source": ""}]}],
            [
                {
                    "link": {
                        "url": "http://localhost/",
                        "source": [{"text": "", "source": ""}],
                    },
                    "result": False,
                    "invalidate_regex": ["local_authority"],
                }
            ],
            id="Local URL without https protocol.",
        ),
        param(
            [{"url": "https://0.0.0.0/", "source": [{"text": "", "source": ""}]}],
            [
                {
                    "link": {
                        "url": "https://0.0.0.0/",
                        "source": [{"text": "", "source": ""}],
                    },
                    "result": False,
                    "invalidate_regex": ["local_authority"],
                }
            ],
            id="Local IP address with https.",
        ),
        param(
            [{"url": "http:///test.com/", "source": [{"text": "", "source": ""}]}],
            [
                {
                    "link": {
                        "url": "http:///test.com/",
                        "source": [{"text": "", "source": ""}],
                    },
                    "result": False,
                    "invalidate_regex": ["empty_authority"],
                }
            ],
            id="URL with triple slash.",
        ),
        param(
            [{"url": "ftp://test.com/", "source": [{"text": "", "source": ""}]}],
            [
                {
                    "link": {
                        "url": "ftp://test.com/",
                        "source": [{"text": "", "source": ""}],
                    },
                    "result": False,
                    "invalidate_regex": ["https_protocol"],
                }
            ],
            id="URL with ftp protocol.",
        ),
        param(
            [{"url": "toto", "source": [{"text": "", "source": ""}]}],
            [
                {
                    "link": {"url": "toto", "source": [{"text": "", "source": ""}]},
                    "result": False,
                    "invalidate_regex": ["missing_protocol", "missing_double_slash"],
                }
            ],
            id="Just domain name.",
        ),
        param(
            [{"url": "/toto/titi/index.html", "source": [{"text": "", "source": ""}]}],
            [
                {
                    "link": {
                        "url": "/toto/titi/index.html",
                        "source": [{"text": "", "source": ""}],
                    },
                    "result": False,
                    "invalidate_regex": [
                        "missing_protocol",
                        "missing_double_slash",
                        "empty_authority",
                    ],
                }
            ],
            id="Absolute path 1.",
        ),
        param(
            [{"url": "//titi.html", "source": [{"text": "", "source": ""}]}],
            [
                {
                    "link": {
                        "url": "//titi.html",
                        "source": [{"text": "", "source": ""}],
                    },
                    "result": False,
                    "invalidate_regex": ["missing_protocol"],
                }
            ],
            id="Absolute path 2.",
        ),
        param(
            [
                {
                    "url": "foo://example.com:8042/over/there?name=ferret#nose",
                    "source": [{"text": "", "source": ""}],
                }
            ],
            [
                {
                    "link": {
                        "url": "foo://example.com:8042/over/there?name=ferret#nose",
                        "source": [{"text": "", "source": ""}],
                    },
                    "result": False,
                    "invalidate_regex": ["https_protocol"],
                }
            ],
            id="Not http(s) protocol - foo protocol.",
        ),
        param(
            [
                {
                    "url": "urn:example:animal:ferret:nose",
                    "source": [{"text": "", "source": ""}],
                }
            ],
            [
                {
                    "link": {
                        "url": "urn:example:animal:ferret:nose",
                        "source": [{"text": "", "source": ""}],
                    },
                    "result": False,
                    "invalidate_regex": ["https_protocol", "missing_double_slash"],
                }
            ],
            id="Not http(s) protocol - URN protocol.",
        ),
        param(
            [{"url": "tel:+1-816-555-1212", "source": [{"text": "", "source": ""}]}],
            [
                {
                    "link": {
                        "url": "tel:+1-816-555-1212",
                        "source": [{"text": "", "source": ""}],
                    },
                    "result": False,
                    "invalidate_regex": ["https_protocol", "missing_double_slash"],
                }
            ],
            id="Not http(s) protocol - tel protocol.",
        ),
        param(
            [
                {
                    "url": "mailto:John.Doe@example.com",
                    "source": [{"text": "", "source": ""}],
                }
            ],
            [
                {
                    "link": {
                        "url": "mailto:John.Doe@example.com",
                        "source": [{"text": "", "source": ""}],
                    },
                    "result": False,
                    "invalidate_regex": ["https_protocol", "missing_double_slash"],
                }
            ],
            id="Not http(s) protocol - mailto protocol.",
        ),
        param(
            [{"url": "https://toto.com/", "source": [{"text": "", "source": ""}]}],
            [
                {
                    "link": {
                        "url": "https://toto.com/",
                        "source": [{"text": "", "source": ""}],
                    },
                    "result": True,
                    "invalidate_regex": [],
                }
            ],
            id="URL with https and domain name.",
        ),
        param(
            [{"url": "http://toto.com/", "source": [{"text": "", "source": ""}]}],
            [
                {
                    "link": {
                        "url": "http://toto.com/",
                        "source": [{"text": "", "source": ""}],
                    },
                    "result": True,
                    "invalidate_regex": [],
                }
            ],
            id="URL with http and domain name.",
        ),
        param(
            [
                {
                    "url": "https://toto.com/path?query#fragment",
                    "source": [{"text": "", "source": ""}],
                }
            ],
            [
                {
                    "link": {
                        "url": "https://toto.com/path?query#fragment",
                        "source": [{"text": "", "source": ""}],
                    },
                    "result": True,
                    "invalidate_regex": [],
                }
            ],
            id="URL with path, query, and fragment.",
        ),
        param(
            [{"url": "http://a/b/c/d;p?q", "source": [{"text": "", "source": ""}]}],
            [
                {
                    "link": {
                        "url": "http://a/b/c/d;p?q",
                        "source": [{"text": "", "source": ""}],
                    },
                    "result": True,
                    "invalidate_regex": [],
                }
            ],
            id="HTTP URL with absolute path.",
        ),
        param(
            [{"url": "http://example.com:80/", "source": [{"text": "", "source": ""}]}],
            [
                {
                    "link": {
                        "url": "http://example.com:80/",
                        "source": [{"text": "", "source": ""}],
                    },
                    "result": True,
                    "invalidate_regex": [],
                }
            ],
            id="'Basic' URL with port.",
        ),
        param(
            [{"url": "http://192.168.0.1", "source": [{"text": "", "source": ""}]}],
            [
                {
                    "link": {
                        "url": "http://192.168.0.1",
                        "source": [{"text": "", "source": ""}],
                    },
                    "result": True,
                    "invalidate_regex": [],
                }
            ],
            id="IPv4 address.",
        ),
        param(
            [
                {
                    "url": "http://192.168.0.1:2373",
                    "source": [{"text": "", "source": ""}],
                }
            ],
            [
                {
                    "link": {
                        "url": "http://192.168.0.1:2373",
                        "source": [{"text": "", "source": ""}],
                    },
                    "result": True,
                    "invalidate_regex": [],
                }
            ],
            id="IPv4 address with port.",
        ),
        param(
            [
                {
                    "url": "http://2001:db8:3333:4444:5555:6666:7777:8888",
                    "source": [{"text": "", "source": ""}],
                }
            ],
            [
                {
                    "link": {
                        "url": "http://2001:db8:3333:4444:5555:6666:7777:8888",
                        "source": [{"text": "", "source": ""}],
                    },
                    "result": True,
                    "invalidate_regex": [],
                }
            ],
            id="IPv6 address.",
        ),
        param(
            [
                {
                    "url": "http://2001:db8:3333:4444:5555:6666:7777:8888:2373",
                    "source": [{"text": "", "source": ""}],
                }
            ],
            [
                {
                    "link": {
                        "url": "http://2001:db8:3333:4444:5555:6666:7777:8888:2373",
                        "source": [{"text": "", "source": ""}],
                    },
                    "result": True,
                    "invalidate_regex": [],
                }
            ],
            id="IPv6 address with port.",
        ),
        param(
            [{"url": "http://*", "source": [{"text": "", "source": ""}]}],
            [
                {
                    "link": {"url": "http://*", "source": [{"text": "", "source": ""}]},
                    "result": True,
                    "invalidate_regex": [],
                }
            ],
            id="Wildcard URL.",
        ),
        param(
            [{"url": "http://*:2837", "source": [{"text": "", "source": ""}]}],
            [
                {
                    "link": {
                        "url": "http://*:2837",
                        "source": [{"text": "", "source": ""}],
                    },
                    "result": True,
                    "invalidate_regex": [],
                }
            ],
            id="Wildcard URL with port.",
        ),
    ],
)
@pytest.mark.parametrize("validator", [param(UrlValidator)])
def test_check_syntax(url, expected, validator):
    with soft_assertions():
        assert_that(check_syntax(url, validator, None)).is_equal_to(expected)


@pytest.mark.parametrize(
    "url, status_code, expected",
    [
        param(
            [{"url": "http://toto.com", "source": [{"text": "", "source": ""}]}],
            code.value,
            (
                [
                    {
                        "link": {
                            "url": "http://toto.com",
                            "source": [{"text": "", "source": ""}],
                        },
                        "valid": True,
                    }
                ]
                if code in [200, 307]
                else [
                    {
                        "link": {
                            "url": "http://toto.com",
                            "source": [{"text": "", "source": ""}],
                        },
                        "valid": False,
                        "status_code": code.value,
                    }
                ]
            ),
        )
        for code in HTTPStatus
    ],
)
@pytest.mark.parametrize("validator", [param(UrlValidator)])
def test_check_status_code(url, validator, status_code, expected):
    with requests_mock.Mocker() as mocker:
        for link in url:
            mocker.get(link["url"], json={}, status_code=status_code)
            result = check_status_code(
                url, validator, [HTTPStatus.OK, HTTPStatus.TEMPORARY_REDIRECT]
            )
            assert_that(result).is_equal_to(expected)
