# Test imports
import pytest

# Standard imports
from http.client import HTTPConnection
from pathlib import Path
from subprocess import PIPE, Popen
from time import sleep

DUMMY_WEBSITE = Path(__file__).parent / "testing_files/dummy_website"


@pytest.fixture(scope="session")
def server():
    process = Popen(
        ["python", "-m", "http.server", "8100", "--directory", DUMMY_WEBSITE],
        stdout=PIPE,
    )
    retries = 5
    while retries > 0:
        conn = HTTPConnection("localhost:8100")
        try:
            conn.request("HEAD", "/")
            response = conn.getresponse()
            if response is not None:
                yield process
                break
        except ConnectionRefusedError:
            sleep(1)
            retries -= 1

    if not retries:
        raise RuntimeError("Failed to start http server")
    else:
        process.terminate()
        process.wait()
