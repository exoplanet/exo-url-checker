# Test imports
from assertpy import assert_that, soft_assertions

# First party imports
from exourlchecker import UrlValidator, crawl_and_check


def test_crawl_and_check(server, tmp_path):
    test_folder = tmp_path / "test_crawl_and_check"
    test_folder.mkdir()

    result = crawl_and_check(
        ["http://0.0.0.0:8100/"], ["0.0.0.0"], UrlValidator, test_folder / "test.json"
    )

    with soft_assertions():
        assert_that(result).contains_key("valid")
        assert_that(result).contains_key("invalid")

        assert_that(result["valid"]).is_instance_of(list)
        assert_that(result["invalid"]).is_instance_of(list)

        for valid_entry in result["valid"]:
            assert_that(valid_entry).contains_key("link")
            assert_that(valid_entry["link"]).contains_key("url")
            assert_that(valid_entry["link"]).contains_key("source")

            for source in valid_entry["link"]["source"]:
                assert_that(source).contains_key("text")
                assert_that(source).contains_key("source")

        for invalid_entry in result["invalid"]:
            assert_that(invalid_entry).contains_key("link")
            assert_that(invalid_entry["link"]).contains_key("url")
            assert_that(invalid_entry["link"]).contains_key("source")

            for source in invalid_entry["link"]["source"]:
                assert_that(source).contains_key("text")
                assert_that(source).contains_key("source")

            assert_that(invalid_entry).contains_key("reason")

            reason = invalid_entry["reason"]
            if isinstance(reason, list) or isinstance(reason, str):
                assert_that(reason).is_not_empty()
            elif isinstance(reason, int):
                assert_that(reason).is_greater_than_or_equal_to(0)
            else:
                assert_that(reason).is_not_none()
