# Test imports
from assertpy import assert_that, soft_assertions

# Standard imports
from http import HTTPStatus

# First party imports
from exourlchecker import AbstractUrlValidator


def test_abstract_class_raise_error():
    abstract_class = AbstractUrlValidator("https://toto.com", None)
    with soft_assertions():
        assert_that(abstract_class.is_valid_url()).is_equal_to(NotImplemented)
        assert_that(abstract_class.check_requests()).is_equal_to(NotImplemented)
