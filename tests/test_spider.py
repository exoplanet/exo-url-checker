# Test imports
import pytest

# First party imports
from exourlchecker import LinksSpider


def test_init_valid_arguments():
    """Test with valid arguments."""
    start_urls = ["http://example.com", "http://example.org"]
    allowed_domain = ["example.com", "example.org"]

    spider = LinksSpider(start_urls=start_urls, allowed_domain=allowed_domain)

    assert spider.start_urls == start_urls
    assert spider.allowed_domain == allowed_domain
