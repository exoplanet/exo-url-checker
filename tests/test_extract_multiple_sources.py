# Test imports
from assertpy import assert_that, soft_assertions

# First party imports
from exourlchecker import get_multiple_sources


def test_get_multiple_sources():
    result = get_multiple_sources("tests/testing_files/expected_links_for_extract.json")
    with soft_assertions():
        assert_that(result).is_not_empty()
        assert_that(len(result)).is_equal_to(5)
        assert_that(result).contains_only(
            *[
                {
                    "url": "http://0.0.0.0:8100/2.html",
                    "source": [
                        {"text": "page 2", "source": "http://0.0.0.0:8100/"},
                        {"text": "page 2", "source": "http://0.0.0.0:8100/3.html"},
                        {"text": "page 2", "source": "http://0.0.0.0:8100/4.html"},
                        {"text": "page 2", "source": "http://0.0.0.0:8100/index.html"},
                    ],
                },
                {
                    "url": "http://0.0.0.0:8100/3.html",
                    "source": [
                        {"text": "page 3", "source": "http://0.0.0.0:8100/"},
                        {"text": "page 3", "source": "http://0.0.0.0:8100/2.html"},
                        {"text": "page 4", "source": "http://0.0.0.0:8100/4.html"},
                        {"text": "page 3", "source": "http://0.0.0.0:8100/index.html"},
                    ],
                },
                {
                    "url": "http://0.0.0.0:8100/4.html",
                    "source": [
                        {"text": "page 4", "source": "http://0.0.0.0:8100/"},
                        {"text": "page 4", "source": "http://0.0.0.0:8100/2.html"},
                        {"text": "page 4", "source": "http://0.0.0.0:8100/3.html"},
                        {"text": "page 4", "source": "http://0.0.0.0:8100/index.html"},
                    ],
                },
                {
                    "url": "https://exoplanet.eu/",
                    "source": [
                        {"text": "exoplanet.eu", "source": "http://0.0.0.0:8100/"},
                        {
                            "text": "exoplanet.eu",
                            "source": "http://0.0.0.0:8100/2.html",
                        },
                        {
                            "text": "exoplanet.eu",
                            "source": "http://0.0.0.0:8100/3.html",
                        },
                        {
                            "text": "exoplanet.eu",
                            "source": "http://0.0.0.0:8100/4.html",
                        },
                        {
                            "text": "exoplanet.eu",
                            "source": "http://0.0.0.0:8100/index.html",
                        },
                    ],
                },
                {
                    "url": "http://0.0.0.0:8100/index.html",
                    "source": [
                        {"text": "index", "source": "http://0.0.0.0:8100/2.html"},
                        {"text": "index", "source": "http://0.0.0.0:8100/3.html"},
                        {"text": "index", "source": "http://0.0.0.0:8100/4.html"},
                    ],
                },
            ]
        )
