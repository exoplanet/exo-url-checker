# Test imports
import pytest
from assertpy import assert_that, soft_assertions
from pytest import param

# Standard imports
import re
from http import HTTPStatus

# First party imports
from exourlchecker import UrlValidator


@pytest.mark.parametrize("given", [f"{nb}" for nb in range(0, 256)])
def test_UrlValidator_regex__dec_octet__success(given):
    assert (
        re.compile(rf"^{UrlValidator('toto', [HTTPStatus.OK]).dec_octet}$").match(given)
        is not None
    )


@pytest.mark.parametrize(
    "given",
    [
        param("abcd", id="Only letters"),
        param("a0c1", id="Digits and letters"),
        param("-1", id="Digits < 0"),
        param("256", id="Digits > 255"),
    ],
)
def test_UrlValidator_regex__dec_octet__fail(given):
    assert (
        re.compile(rf"^{UrlValidator('toto', [HTTPStatus.OK]).dec_octet}$").match(given)
        is None
    )


@pytest.mark.parametrize(
    "given",
    [
        param("http:", id="given: http"),
        param("https:", id="given: https"),
    ],
)
def test_UrlValidator_regex__https_protocol__success(given):
    assert (
        UrlValidator("toto", [HTTPStatus.OK]).regex_https_protocol.match(given)
        is not None
    )


@pytest.mark.parametrize(
    "given",
    [
        param("http", id="given: http missing ':'"),
        param("https", id="given: https missing ':'"),
        param("https:qe", id="given: https in other letters - letters at the end"),
        param("qehttps:", id="given: https in other letters - letters at the start"),
        param("httqeps:", id="given: https in other letters - letters at the middle"),
        param("http:qe", id="given: http in other letters - letters at the end"),
        param("qehttp:", id="given: http in other letters - letters at the start"),
        param("httqep:", id="given: http in other letters - letters at the middle"),
        param("aaaa:", id="given: random letters"),
        param("0000:", id="given: digits"),
        param("aa01a:", id="given: digits and letters"),
        param("ftp:", id="given: ftp protocol"),
        param("urn:", id="given: urn protocol"),
        param("mailto:", id="given: mailto protocol"),
    ],
)
def test_UrlValidator_regex__https_protocol__fail(given):
    assert (
        UrlValidator("toto", [HTTPStatus.OK]).regex_https_protocol.match(given) is None
    )


@pytest.mark.parametrize(
    "given",
    [
        param("localhost:8000", id="localhost with port"),
        param("localhost", id="localhost without port"),
        param("127.0.0.1:8000", id="127.dec.dec.dec with port"),
        param("127.0.0.1", id="127.dec.dec.dec without port"),
        param("0.0.0.0:8000", id="0.0.0.0 with port"),
        param("0.0.0.0", id="0.0.0.0 without port"),
        param("::1:8000", id="::1 with port"),
        param("::1", id="::1 without port"),
    ],
)
def test_UrlValidator_regex__local_url__succes(given):
    assert (
        UrlValidator("toto", [HTTPStatus.OK]).regex_local_url.match(given) is not None
    )


@pytest.mark.parametrize(
    "given",
    [
        param("aaaa", id=""),
        param("00000", id=""),
        param("aa00a0a", id=""),
        param("192.168.0.1", id="ipv4 authority"),
        param("2001:db8:3333:4444:5555:6666:7777:8888", id="ipv6 address"),
        param("John.Doe@example", id="mail authority"),
        param("+1-816-555-1212", id="tel authority"),
    ],
)
def test_UrlValidator_regex__local_url__fail(given):
    assert UrlValidator("toto", [HTTPStatus.OK]).regex_local_url.match(given) is None


@pytest.mark.parametrize(
    "given",
    [
        # False
        param("https://localhost/", id="Local url with localhost"),
        param("http://localhost:88888/", id="Local url with localhost and port"),
        param("http://0.0.0.0/", id="Local url with ip address"),
        param("http://127.0.0.1:99999/", id="Local url with ip address and port"),
        param("http:///test.com/", id="Url with triple slash"),
        param("ftp://test.com/", id="Url with ftp protocol"),
        param("", id="Empty link"),
        param("toto", id="Just domaine name"),
        param("/toto/titi/index.html", id="Absolute path 1"),
        param("//titi.html", id="Absolute path 2"),
        param(
            "foo://example.com:8042/over/there?name=ferret#nose",
            id="Not http(s) protocol - foo protocol",
        ),
        param(
            "urn:example:animal:ferret:nose", id="Not http(s) protocol urntel protocol"
        ),
        param("tel:+1-816-555-1212", id="Not http(s) protocol - tel protocol"),
        param(
            "mailto:John.Doe@example.com", id="Not http(s) protocol - mailto protocol"
        ),
        # True
        param("https://toto.com/", id="Url with https and domaine name"),
        param("http://toto.com/", id="Url with http and not https"),
        param(
            "https://toto.com/path?query#fragment",
            id="Url with path, query and fragment",
        ),
        param("http://a/b/c/d;p?q", id="http(s) protocole - absolute path"),
        param("http://example.com:80/", id="'basic' url with port"),
        param("http://192.168.0.1", id="ipv4 adress"),
        param("http://192.168.0.1:2373", id="ipv4 adress with port"),
        param("http://2001:db8:3333:4444:5555:6666:7777:8888", id="ipv6 adress"),
        param(
            "http://2001:db8:3333:4444:5555:6666:7777:8888:2373",
            id="ipv6 adress with port",
        ),
        param(
            "http://*",
            id="All",
        ),
        param(
            "http://*:2837",
            id="All with port",
        ),
    ],
)
def test_UrlValidator_regex__local_url(given):
    assert UrlValidator("toto", [HTTPStatus.OK]).regex_url.match(given) is not None


@pytest.mark.parametrize(
    "urls, expected_result, expected_invalidate_regex",
    [
        # False
        param(
            [{"url": "https://localhost/", "source": [{"text": "", "source": ""}]}],
            False,
            ["local_authority"],
            id="Local URL with localhost",
        ),
        param(
            [
                {
                    "url": "http://localhost:88888/",
                    "source": [{"text": "", "source": ""}],
                }
            ],
            False,
            ["local_authority"],
            id="Local URL with localhost and port",
        ),
        param(
            [{"url": "http://0.0.0.0/", "source": [{"text": "", "source": ""}]}],
            False,
            ["local_authority"],
            id="Local URL with IP address",
        ),
        param(
            [
                {
                    "url": "http://127.0.0.1:99999/",
                    "source": [{"text": "", "source": ""}],
                }
            ],
            False,
            ["local_authority"],
            id="Local URL with IP address and port",
        ),
        param(
            [{"url": "http:///test.com/", "source": [{"text": "", "source": ""}]}],
            False,
            ["empty_authority"],
            id="URL with triple slash",
        ),
        param(
            [{"url": "ftp://test.com/", "source": [{"text": "", "source": ""}]}],
            False,
            ["https_protocol"],
            id="URL with FTP protocol",
        ),
        param(
            [{"url": "", "source": [{"text": "", "source": ""}]}],
            False,
            ["missing_protocol", "missing_double_slash", "empty_authority"],
            id="Empty link",
        ),
        param(
            [{"url": "toto", "source": [{"text": "", "source": ""}]}],
            False,
            ["missing_protocol", "missing_double_slash"],
            id="Just domain name",
        ),
        param(
            [{"url": "/toto/titi/index.html", "source": [{"text": "", "source": ""}]}],
            False,
            ["missing_protocol", "missing_double_slash", "empty_authority"],
            id="Absolute path 1",
        ),
        param(
            [{"url": "//titi.html", "source": [{"text": "", "source": ""}]}],
            False,
            ["missing_protocol"],
            id="Absolute path 2",
        ),
        param(
            [
                {
                    "url": "foo://example.com:8042/over/there?name=ferret#nose",
                    "source": [{"text": "", "source": ""}],
                }
            ],
            False,
            ["https_protocol"],
            id="Not http(s) protocol - foo protocol",
        ),
        param(
            [
                {
                    "url": "urn:example:animal:ferret:nose",
                    "source": [{"text": "", "source": ""}],
                }
            ],
            False,
            ["https_protocol", "missing_double_slash"],
            id="Not http(s) protocol - URN protocol",
        ),
        param(
            [{"url": "tel:+1-816-555-1212", "source": [{"text": "", "source": ""}]}],
            False,
            ["https_protocol", "missing_double_slash"],
            id="Not http(s) protocol - tel protocol",
        ),
        param(
            [
                {
                    "url": "mailto:John.Doe@example.com",
                    "source": [{"text": "", "source": ""}],
                }
            ],
            False,
            ["https_protocol", "missing_double_slash"],
            id="Not http(s) protocol - mailto protocol",
        ),
        param(
            [{"test": "http://valid.url"}],
            False,
            ["link is not a dict or missing url"],
            id="Match is None",
        ),
        param(
            [{"url": 12345}],
            False,
            ["url is not a string"],
            id="url is not a string",
        ),
        # True
        param(
            [{"url": "https://toto.com/", "source": [{"text": "", "source": ""}]}],
            True,
            [],
            id="URL with https and domain name",
        ),
        param(
            [{"url": "http://toto.com/", "source": [{"text": "", "source": ""}]}],
            True,
            [],
            id="URL with http and domain name",
        ),
        param(
            [
                {
                    "url": "https://toto.com/path?query#fragment",
                    "source": [{"text": "", "source": ""}],
                }
            ],
            True,
            [],
            id="URL with path, query, and fragment",
        ),
        param(
            [{"url": "http://a/b/c/d;p?q", "source": [{"text": "", "source": ""}]}],
            True,
            [],
            id="HTTP URL with absolute path",
        ),
        param(
            [{"url": "http://example.com:80/", "source": [{"text": "", "source": ""}]}],
            True,
            [],
            id="Basic URL with port",
        ),
        param(
            [{"url": "http://192.168.0.1", "source": [{"text": "", "source": ""}]}],
            True,
            [],
            id="IPv4 address",
        ),
        param(
            [
                {
                    "url": "http://192.168.0.1:2373",
                    "source": [{"text": "", "source": ""}],
                }
            ],
            True,
            [],
            id="IPv4 address with port",
        ),
        param(
            [
                {
                    "url": "http://2001:db8:3333:4444:5555:6666:7777:8888",
                    "source": [{"text": "", "source": ""}],
                }
            ],
            True,
            [],
            id="IPv6 address",
        ),
        param(
            [
                {
                    "url": "http://2001:db8:3333:4444:5555:6666:7777:8888:2373",
                    "source": [{"text": "", "source": ""}],
                }
            ],
            True,
            [],
            id="IPv6 address with port",
        ),
        param(
            [{"url": "http://*", "source": [{"text": "", "source": ""}]}],
            True,
            [],
            id="Wildcard URL",
        ),
        param(
            [{"url": "http://*:2837", "source": [{"text": "", "source": ""}]}],
            True,
            [],
            id="Wildcard URL with port",
        ),
    ],
)
def test_url_checker(urls, expected_result, expected_invalidate_regex):
    validator = UrlValidator(urls, None)
    result = validator.is_valid_url()

    with soft_assertions():
        assert_that(result[0]["result"]).is_equal_to(expected_result)
        assert_that(result[0]["invalidate_regex"]).is_equal_to(
            expected_invalidate_regex
        )
