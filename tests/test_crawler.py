# Test imports
from assertpy import assert_that, soft_assertions

# Standard imports
from json import load as json_load

# First party imports
from exourlchecker import crawl_and_save


def test_crawl_and_save(server, tmp_path):

    test_folder = tmp_path / "test_crawl_and_save"
    test_folder.mkdir()

    crawl_and_save(["http://0.0.0.0:8100/"], ["0.0.0.0"], test_folder / "test.json")

    with open(test_folder / "test.json", "r") as file:
        data = json_load(file)

    with soft_assertions():
        assert_that(len(data)).is_equal_to(19)
        for link in data:
            assert_that(list(link.keys())).contains_only("url", "text", "source")
