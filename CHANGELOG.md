<!--

EXEMPLE de Changelog :

# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [TODO] <<< les choses à faire (logique)

## [Unreleased] <<< les choses en cours donnant une prochaine version

## [0.0.1] - 2014-05-31 <<< Quand tu changes de version, pense a changer aussi
le pyproject.toml

### Added / Changed / Deleted <<< l'un des trois, pas les trois. Il peut y
avoir plusieurs mais successivement.

-  -->

# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Added

- Add function analyze to crawl and check in one function.
- Add function to get an expected or unexpected status_code.
- Add URL validator.
- Add a function to extract links and brings together all the sources for the same url.
- Add a function to extract links in the script.
- Add a script to recover the crawler.
- Add crawler to get all link in a webiste.
