# ExoUrlChecker

Last update : [2024-08-21]

This tool allows you to check the syntax and validity of the links
found on the given websites, extracts them, and displays them with
the location where the link was found on the websites.

## Contact

- Author: Rakulan SIVATHASAN
- Maintainer: Rakulan SIVATHASAN
- Email: [s.rakulan04@gmail.com](mailto:s.rakulan04@gmail.com)
- Contributors:
  - Ulysse CHOSSON (LESIA)
  - Pierre-Yves MARTIN (LESIA)

## Requirement

For the proper functioning of this project, please install some tools before starting:

- [Poetry](https://python-poetry.org/docs/)
- [Just](https://github.com/casey/just)

## How to install

Clone the repository, go to the folder and launch the virtual environment:

```sh
git clone https://gitlab.obspm.fr/exoplanet/exo-url-checker.git
cd exo-url-checker/
poetry shell
```

and run the command below to install the dependencies :

```sh
poetry install --no-dev
```

## How to use

Call the crawl_and_check function in the main.py or another file and
enter as parameters a URL where you want it to start, a list of allowed
domains on which the crawler can pass, the validator, and the output
file to store all the links.

```sh
from exourlchecker import crawl_and_check

crawl_and_check(
    ["https://example.com"], ["example.com", "example2.com"], UrlValidator, "links.json"
)
```

```sh
python ./main.py
```

## Documentation

- [scrapy](https://docs.scrapy.org/en/latest/index.html)
