set dotenv-load := false

# List all just commands
list:
    just --list --unsorted

# Remove all python artefacts files and folders
@clean:
    printf '%-40s' 'Cleaning python artefact: '
    find . \( -name __pycache__ -o -name "*.pyc" \) -delete
    echo '✅'
    printf '%-40s' 'Cleaning pytest artefact: '
    rm -rf .pytest_cache
    echo '✅'

# Uninstall pre-commit hooks
@_uninstall-pre-commit:
    printf '%-40s' 'Uninstall pre-commit hooks: '
    pre-commit uninstall > /dev/null
    pre-commit clean > /dev/null
    pre-commit gc > /dev/null
    echo '✅'


# Launch mypy
@mypy-exourlchecker:
    mypy --pretty -p exourlchecker --config-file pyproject.toml
    echo 'Mypy exourlchecker: ✅'

# Launch ruff
ruff path ="exourlchecker":
    ruff check {{path}}
    echo 'Ruff ✅'


# Run markdownlint
lintmd path='"**/*.md"': clean
    markdownlint --config ".markdownlint.yaml" {{ path }}

# Run black and isort
@onmy31 path="exourlchecker tests": clean
    black {{ path }}
    echo 'black: ✅'
    isort {{ path }}
    echo 'isort: ✅'

# Run all linter
lint: clean onmy31 mypy-exourlchecker ruff
