"""Get all links and check."""

# Standard imports
from http import HTTPStatus
from pathlib import Path
from typing import Any, cast

# Local imports
from .analyze import check_status_code, check_syntax
from .crawler import crawl_and_save, get_multiple_sources
from .url_validator import AbstractUrlValidator


def crawl_and_check(
    start_urls: str,
    allowed_domain: str,
    validator: type[AbstractUrlValidator],
    outputfile: Path,
) -> Any:  # noqa: ANN401
    """
    Crawl websites, save links and check if it's correct and it's work.

    Returns Any because we are calling an abstract class which returns Any

    Args:
        start_urls: Where the crawler start.
        allowed_domain: Where the crawler can access.
        validator: Validator used to validate the given url.
        outputfile: File to save the crawler result.

    Returns:
        Url, sources and reason if the syntax or http status code failed.
    """
    valid = []
    invalid = []

    crawl_and_save(start_urls, allowed_domain, outputfile)
    all_links = get_multiple_sources(outputfile)

    result_syntax = check_syntax(
        cast(
            dict[str, list[dict[str, str]]] | list[dict[str, list[dict[str, str]]]],
            all_links,
        ),
        validator,
        [HTTPStatus.OK, HTTPStatus.TEMPORARY_REDIRECT],
    )

    for result in result_syntax:
        if not result["result"]:
            invalid.append(
                {
                    "link": result["link"],
                    "reason": result["invalidate_regex"],
                }
            )
        else:
            result_http = check_status_code(
                result["link"],
                validator,
                [HTTPStatus.OK, HTTPStatus.TEMPORARY_REDIRECT],
            )[0]
            if not result_http["valid"]:
                reason = result_http.get(
                    "status_code", result_http.get("error", "Unknown error")
                )
                invalid.append(
                    {
                        "link": result["link"],
                        "reason": reason,
                    }
                )
            else:
                valid.append({"link": result["link"]})

    return {"valid": valid, "invalid": invalid}
