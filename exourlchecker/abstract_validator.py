"""Module for abstract URL validation."""

# Standard imports
from http import HTTPStatus
from typing import Any


class AbstractUrlValidator:
    """Abstract base class for URL validation."""

    url: dict[str, list[dict[str, str]]] | list[dict[str, list[dict[str, str]]]]
    """URL to validate."""

    allowed_code: list[HTTPStatus] | None
    """Allowed code to check URL"""

    def __init__(
        self,
        url: dict[str, list[dict[str, str]]] | list[dict[str, list[dict[str, str]]]],
        allowed_code: list[HTTPStatus] | None,
    ) -> None:
        """Initialize the class with a URL."""
        self.url = url
        self.allowed_code = allowed_code

    # Ignore ruff error it is an abstract class that is only used for inheritance
    def is_valid_url(self) -> Any:  # noqa: ANN401
        """Check if the URL is valid."""
        return NotImplemented

    # Ignore ruff error it is an abstract class that is only used for inheritance
    def check_requests(self) -> Any:  # noqa: ANN401
        """Check the URL status code."""
        return NotImplemented
