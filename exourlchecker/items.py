"""New item for the crawler."""

# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

# Third party imports
import scrapy


class ExourlcheckerItem(scrapy.Item):
    """Define new item for the scrawler."""

    # define the fields for your item here like:
    name = scrapy.Field()
    pass
