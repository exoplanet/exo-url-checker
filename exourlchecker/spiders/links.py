"""Crawler of the project."""

# Standard imports
import collections.abc
from typing import Any

# Third party imports
import scrapy
import scrapy.http
import scrapy.http.response
from scrapy.linkextractors import LinkExtractor


class LinksSpider(scrapy.Spider):
    """Class LinksSpider to get all links in specific domain."""

    name = "LinksSpider"
    """Name of the spider."""

    start_urls: list[str]
    """Start urls for the spider."""

    allowed_domain: list[str]
    """All domains where the crawler can access."""

    link_extractor = LinkExtractor(allow=())
    """Extractor for link in response."""

    def __init__(self, start_urls: list[str], allowed_domain: list[str]) -> None:
        """
        Initilize method of the class.

        Args:
            start_urls: Where the crawler start.
            allowed_domain: Where the crawler can access.
        """
        assert isinstance(start_urls, list) and all(
            isinstance(url, str) for url in start_urls
        )
        assert isinstance(allowed_domain, list) and all(
            isinstance(domain, str) for domain in allowed_domain
        )

        self.start_urls = start_urls
        self.allowed_domain = allowed_domain

    def parse(  # pragma: no cover # Because it's hard to test this function with scrapy
        self,
        response: scrapy.http.response,
    ) -> collections.abc.Generator[Any, Any, Any]:
        """
        Parse urls of a response.

        Args:
            response: Response to parse.
        """
        wrong = False
        for link in self.link_extractor.extract_links(response):
            for domain in self.allowed_domain:
                if domain not in link.url:
                    wrong = True
            if not wrong:
                yield response.follow(link.url, self.parse)
            yield {"url": link.url, "text": link.text, "source": response.url}
