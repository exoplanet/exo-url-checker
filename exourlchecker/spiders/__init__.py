"""Scrapy spiders module."""

# This package will contain the spiders of your Scrapy project
#
# Please refer to the documentation for information on how to create and manage
# your spiders.

# Local imports
from .links import LinksSpider  # noqa: F401
