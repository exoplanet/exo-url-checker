"""Module to check URL."""

# Standard imports
import re
from http import HTTPStatus
from typing import cast

# Third party imports
import requests

# Local imports
from .abstract_validator import AbstractUrlValidator


class UrlValidator(AbstractUrlValidator):
    """UrlValidator class for check url syntax."""

    dec_octet = r"([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])"
    """For (0 to 9 | 10 to 99 | 100 to 199 | 200 to 249 | 250 to 255)."""

    regex_url = re.compile(
        r"^(?P<protocol>(([^:/?#]+):)?)(?P<double_slash>\/\/)?(?P<authority>([^/?#]*))?"
        r"(?P<path>([^?#]*))(?P<query>(\?([^#]*)))?(?P<fragment>(#(.*)))?$"
    )
    """Match an URI."""

    regex_https_protocol = re.compile(r"^https?:$")
    """Regex to verifiy the protocol."""

    regex_local_url = re.compile(
        rf"^(localhost|0\.0\.0\.0|127\.{dec_octet}\.{dec_octet}\.{dec_octet}|::1?)"
        r"(:[0-9]*)?$"
    )
    """Regex to verify if is it's a local url."""

    links: list[dict[str, str | list[dict[str, str]]]]
    """URL to validate."""

    allowed_code: list[HTTPStatus] | None
    """Allowed code to check URL"""

    # TODO: Unable to take multi url

    def __init__(
        self,
        links: (
            dict[str, str | list[dict[str, str]]]
            | list[dict[str, str | list[dict[str, str]]]]
        ),
        allowed_code: list[HTTPStatus] | None,
    ) -> None:
        """
        Initialize the UrlValidator class.

        Args:
            links: One or more URLs whose syntax must be checked.
            allowed_code: Allowed status code.
        """
        self.allowed_code = allowed_code
        self.links = links if isinstance(links, list) else [links]

    def is_empty_or_none(self, string_to_check: str | None) -> bool:
        """
        Check if entry is empty or None.

        Args:
            string_to_check: Entry to check.

        Returns:
            True if given string is empty or equal to None, False otherwise.

        Examples:
            >>> url_checker = UrlValidator("toto", [HTTPStatus.OK])
            >>> url_checker.is_empty_or_none(None)
            True

            >>> url_checker.is_empty_or_none('')
            True

            >>> url_checker.is_empty_or_none('test')
            False
        """
        return string_to_check is None or string_to_check == ""

    def is_valid_url(
        self,
    ) -> list[dict[str, object]]:
        """
        Check if the url is correct.

        Returns:
            True or False with errors.

        Examples:
            >>> UrlValidator(
            ...     [{"url": "http://localhost",
            ...         "source": [{"text": "", "source": ""}]}],
            ...     [HTTPStatus.OK]
            ... ).is_valid_url()
            [{'link':
            ... {'url': 'http://localhost', 'source': [{'text': '', 'source': ''}]},
            ... 'result': False,
            ... 'invalidate_regex': ['local_authority']
            ... }]

            >>> UrlValidator(
            ...     [{"url": "https://exoplanet.eu",
            ...         "source": [{"text": "", "source": ""}]}],
            ...     [HTTPStatus.OK]
            ... ).is_valid_url()
            [{'link':
            ... {'url': 'https://exoplanet.eu', 'source': [{'text': '', 'source': ''}]},
            ... 'result': True,
            ... 'invalidate_regex': []
            ... }]

            >>> UrlValidator(
            ...     [{"url": "https://exoplanet.eu", "text": "", "source": ""},
            ...         {"url": "https://google.com", "text": "", "source": ""}],
            ...     [HTTPStatus.OK]
            ... ).is_valid_url()
            [{'link':
            ... {'url': 'https://exoplanet.eu', 'text': '', 'source': ''},
            ... 'result': True,
            ... 'invalidate_regex': []},
            ... {'link':
            ... {'url': 'https://google.com', 'text': '', 'source': ''},
            ... 'result': True,
            ... 'invalidate_regex': []
            ... }]
        """
        results = []
        for link in self.links:
            if isinstance(link, dict) and "url" in link:
                url = link["url"]
                if isinstance(url, str):
                    match = self.regex_url.match(url)
                    if match is None:  # pragma: no cover # Because it's just a security
                        results.append(
                            {
                                "link": link,
                                "result": False,
                                "invalidate_regex": ["invalid_url"],
                            }
                        )
                        continue

                    groups = match.groupdict()
                    protocol = groups.get("protocol")
                    double_slash = groups.get("double_slash")
                    authority = groups.get("authority")

                    invalidate_regex = []

                    if self.is_empty_or_none(protocol):
                        invalidate_regex.append("missing_protocol")
                    elif not self.regex_https_protocol.match(cast(str, protocol)):
                        invalidate_regex.append("https_protocol")

                    if self.is_empty_or_none(double_slash):
                        invalidate_regex.append("missing_double_slash")

                    if self.is_empty_or_none(authority):
                        invalidate_regex.append("empty_authority")
                    elif self.regex_local_url.match(cast(str, authority)):
                        invalidate_regex.append("local_authority")

                    results.append(
                        {
                            "link": link,
                            "result": len(invalidate_regex) == 0,
                            "invalidate_regex": invalidate_regex,
                        }
                    )
                else:
                    results.append(
                        {
                            "link": link,
                            "result": False,
                            "invalidate_regex": ["url is not a string"],
                        }
                    )
            else:
                results.append(
                    {
                        "link": link,
                        "result": False,
                        "invalidate_regex": ["link is not a dict or missing url"],
                    }
                )
        return results

    # return type : list[dict[str, bool | int | str | dict[str, str |
    # list[dict[str, str]]] | tuple[int, str, str]]]
    def check_requests(
        self,
    ) -> list[dict[str, object]]:
        """
        Check if the URL gives the expected status code.

        Returns:
            Information about the URL's status.
        """
        if self.allowed_code is None:
            raise ValueError("allowed_code cannot be None")

        results = []
        for link in self.links:
            try:
                response = requests.get(
                    cast(str, link["url"]), verify=False, timeout=10
                )
                if response.status_code in self.allowed_code:
                    results.append({"link": link, "valid": True})
                else:
                    results.append(
                        {
                            "link": link,
                            "valid": False,
                            "status_code": response.status_code,
                        }
                    )
            except requests.Timeout:
                results.append(
                    {
                        "link": link,
                        "valid": False,
                        "status_code": HTTPStatus.REQUEST_TIMEOUT,
                    }
                )
            except requests.RequestException as err:
                results.append({"link": link, "valid": False, "error": str(err)})

        return results
