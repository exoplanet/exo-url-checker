"""Module to analyze URL using UrlValidator."""

# Standard imports
from http import HTTPStatus
from typing import Any

# Local imports
from .url_validator import AbstractUrlValidator


def check_syntax(
    url: dict[str, list[dict[str, str]]] | list[dict[str, list[dict[str, str]]]],
    validator: type[AbstractUrlValidator],
    allowed_code: list[HTTPStatus],
) -> Any:  # noqa: ANN401
    """
    Analyze syntax of a given URL.

    Returns Any because we are calling an abstract class which returns Any

    Args:
        url: The URL that will be analyzed.
        validator : Validator used to validate syntax of the given url.
        allowed_code: HTTP status codes considered valid.

    Returns:
        The syntax of the url is correct.
    """
    return validator(url, None).is_valid_url()


def check_status_code(
    url: dict[str, list[dict[str, str]]] | list[dict[str, list[dict[str, str]]]],
    validator: type[AbstractUrlValidator],
    allowed_code: list[HTTPStatus],
) -> Any:  # noqa: ANN401
    """
    Check the status code of a given URL.

    Returns Any because we are calling an abstract class which returns Any

    Args:
        url: The URL to be checked.
        validator : Validator used to validate status code of requests on the given url.
        allowed_code: HTTP status codes considered valid.

    Returns:
        The status code of the url matches the http code we allow.
    """
    return validator(url, allowed_code).check_requests()
