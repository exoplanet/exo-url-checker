"""Make crawler and link extractor."""

# Standard imports
from json import load as json_load
from multiprocessing import Process
from pathlib import Path

# Third party imports
from scrapy.crawler import CrawlerProcess

# Local imports
from .spiders import LinksSpider


# TODO take setting form setting.py file (get_project_settings())
def crawl_and_save(start_urls: str, allowed_domain: str, outputfile: Path) -> None:
    """
    Crawl websites and save links in a json file.

    Args:
        start_urls: Where the crawler start.
        allowed_domain: Where the crawler can access.
        outputfile: File to save result.
    """

    def crawl() -> None:  # pragma: no cover # Because it's hard to test this function
        # with scrapy
        settings = {
            "FEEDS": {
                f"{outputfile}": {"format": "json"},
            },
            "REQUEST_FINGERPRINTER_IMPLEMENTATION": "2.7",
        }
        scrapy_process = CrawlerProcess(settings=settings)

        scrapy_process.crawl(
            LinksSpider,
            start_urls=start_urls,
            allowed_domain=allowed_domain,
        )
        scrapy_process.start()

    process = Process(target=crawl)
    process.start()
    process.join()


def get_links(file: Path) -> list:
    """
    Extract data from a file.

    Args:
        file: File containing the data.

    Returns:
        Data from the given file.
    """
    with open(file) as given_file:
        return list(json_load(given_file))


def get_multiple_sources(file: Path) -> list[dict[str, int]]:
    """
    Brings together all the sources for the same url.

    Args:
        file: File containing the data.

    Returns:
        Sorted data
    """
    sorted_data: dict = {}

    result: list = []

    data = get_links(file)

    for item in data:
        url, text, source = item.values()
        if url not in sorted_data:
            sorted_data[url] = []

        sorted_data[url].append(
            {
                "text": text,
                "source": source,
            }
        )

    for key, value in sorted_data.items():
        result.append({"url": key, "source": value})

    return result
